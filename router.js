const router = require("express").Router();
const userModel = require("./models/user.model");

router.get("/login", (req, res) => {
    res.send("User logged in successfully!")
})

router.post("/register", (req, res) => {
    let  new_user = new userModel(
        {
           name: req.body.name,
           email: req.body.email,
           age: req.body.age 
        }
    )

    new_user.save((err, data) => {
        if(err){
            console.log(err);
            res.send({message: "Failed to save data"})
        } else {
            res.send({message: "Registered successfully!"});
        }
    })
})

router.get("/allusers", (req, res) => {
    userModel.find((err, data) => {
        if(err){
            console.log(err);
            res.send("An error occured!")
        } else {
            res.send(data)
        }
    })
})

router.get("/particular-user", (req, res) => {
    console.log(req.query)
    userModel.findById(req.query._id, (err, data) => {
        if(err){
            console.log(err);
            res.send("An error occured")
        } else {
            res.send(data)
        }
    })
})

module.exports = router;