const express = require("express");
const bodyParser = require("body-parser");
const userRouter = require("./router");
const mongoose = require("mongoose");
const cors = require("cors");

// mongoose.connect("mongodb://localhost:27017/test_db");
mongoose.connect("mongodb://Amin:amin@ds237120.mlab.com:37120/lastclass")

const app = express();

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use("/user", userRouter);

app.get("/", (req, res) => {
    res.send("This is the home. Welcome!")
})

app.post("/sum", (req, res) => {
    let sum = Number(req.query.num_1) + Number(req.query.num_2);
    res.send(sum + "")
})

app.get("/test", (req, res) => {
    res.send("Test works")
});

app.get("*", (req, res) => {
    console.log(req.query.language)
    res.send("404 PAGE NOT FOUND!")
})


app.listen(3009, console.log("App works on port 3009! Hurray!!!"))